from st3m.application import Application, ApplicationContext
from st3m.ui.view import BaseView, ViewManager
from st3m.input import InputState, InputController
from st3m.ui.colours import GREEN
from ctx import Context
import st3m.run
import random
import leds
import bl00mbox

score = 0
new_game = True
petal_leds = {0: [37, 38, 39, 0, 1, 2, 3],
              1: [2,3,4,5,6,7],
              2: [5,6,7,8,9,10,11],
              3: [10,11,12,13,14,15],
              4: [13,14,15,16,17,18,19],
              5: [18,19,20,21,22,23],
              6: [21,22,23,24,25,26,27],
              7: [26,27,28,29,30,31],
              8: [29,30,31,32,33,34,35],
              9: [34,35,36,37,38,39],
              -1: list(range(40)),
              -2: [32, 33]}

blm = bl00mbox.Channel("Bopit")

class GameOverScreen(BaseView):
    def __init__(self):
        super().__init__()

        self.synth = blm.new(bl00mbox.patches.tinysynth)
        self.synth.signals.output = blm.mixer
        self.synth.signals.attack = 50
        self.synth.signals.decay = 50
        self.synth.signals.pitch.tone = 5

        self.timer = 0


    def draw(self, ctx: Context) -> None:
        ctx.rgb(0,0,0).rectangle(-120, -120, 240, 240).fill()
        ctx.font_size = 50
        ctx.font = ctx.get_font_name(5)
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.move_to(60, -20)
        ctx.rgb(255,0,0).text("GAME OVER" + "\n" + "Score: " + str(score))

    def think(self, ins: InputState, delta_ms: int) -> None:
        self.input.think(ins, delta_ms)
        self.timer += delta_ms

        if self.timer >= 800:
            self.synth.signals.trigger.stop()

    def on_enter(self, vm: Optional[ViewManager]):
        self.timer = 0

        self.synth.signals.trigger.start()
        super().on_enter(vm)

    def on_exit(self):
        global score, new_game

        # Reset the game state
        score = 0
        new_game = True
        
        leds.set_all_rgb(0,0,0)
        leds.update()

        self.synth.signals.trigger.stop()

class BopIt(Application):
    current_instruction_timer = 0
    current_instruction = None

    def __init__(self, app_ctx: ApplicationContext) -> None:
        global new_game

        # Ignore the app_ctx for now.
        super().__init__(app_ctx)
        self.input = InputController()
        self.score = 0
        self.current_instruction = Instruction(None) 
        new_game = False
        self.interval = 2500
        self.synth = blm.new(bl00mbox.patches.tinysynth)
        self.synth.signals.output = blm.mixer
        self.synth.signals.attack = 50
        self.synth.signals.decay = 50
        self.synth.signals.pitch.tone = 15
        self.failed = False



    def draw(self, ctx: Context) -> None:
        ctx.rgb(0,0,0).rectangle(-120, -120, 240, 240).fill()

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 60
        ctx.font = ctx.get_font_name(5)
        ctx.move_to(60, -30)
        ctx.rgb(*GREEN).text(self.current_instruction.word + " IT\n" + "Score: " + str(score))

        ctx.restore()

    def think(self, ins: InputState, delta_ms: int) -> None:
        global score, new_game

        super().think(ins, delta_ms) # Let Application do its thing

        if new_game:
            self.score = 0
            self.current_instruction = Instruction(None)    
            new_game = False
            self.interval = 2500
            self.failed = False


        # Update how long this instruction has been active
        self.current_instruction_timer += delta_ms

        if self.current_instruction_timer >= 100:
            self.synth.signals.trigger.stop()

        if self.current_instruction_timer >= self.interval:
            self.failed = True
            self.current_instruction_timer = 0
            leds.set_all_rgb(1.0,0,0)
            leds.update()
            self.vm.push(GameOverScreen())


        # Check if instruction has been followed correctly
        # If instruction has been followed, update score, reset timer, make new instruction
        if self.current_instruction.test(self, ins) and not self.failed:
            score += 1
            self.current_instruction_timer = 0
            leds.set_all_rgb(0,0,0)
            self.current_instruction = self.get_instruction()
            self.interval = max(self.interval-100, 1200)

            self.synth.signals.trigger.start()

    def get_instruction(self):
        # Generate an instruction
        next_instruction = Instruction(self.current_instruction.word)

        return next_instruction


class Instruction():

    def __init__(self, last_word):
        actions = [("TAP", list(range(10)), self.tap_test, 0),
                ("SWIPE", list(range(10)), self.swipe_test, 1),
                ("RUB", list(range(0,10,2)), self.rub_test, 5),
                ("HOLD", list(range(10)), self.hold_test, 3),
                ("SHAKE", [-1], self.shake_test, 4), 
                ("FLICK", [-2], self.flick_test, 2),
                ("CLICK", [-2], self.click_test, 6)]
        
        self.word, petal_choice, self.test, self.colour = random.choice([a for a in actions if a[0] != last_word])
        self.petal = random.choice(petal_choice)
        self.colour *= 36
        
        self.state = 0
        self.timer = 0.0

        # Light up the thing that needs pressing
        for p in petal_leds[self.petal]:
            leds.set_hsv(p, self.colour, 1.0, 1.0)

        leds.update()

    def click_test(self, bop, ins):
        if bop.input.buttons.app.middle.pressed:
            return True
        
        return False
    
    def flick_test(self, bop, ins):
        if bop.input.buttons.app.left.pressed:
            return True
    
        if bop.input.buttons.app.right.pressed:
            return True
        
        return False
    

    def tap_test(self, bop, ins):
        petal = ins.captouch.petals[self.petal]

        if self.state==0 and petal.pressed:
            self.state = 1
        
        if self.state==1 and petal.pressed==False:
            self.state = 0
            return True
        
        return False
    
    def hold_test(self, bop, ins):
        petal = ins.captouch.petals[self.petal]

        if self.state==0 and petal.pressed:
            self.timer = bop.current_instruction_timer
            self.state = 1

        if self.state==1 and petal.pressed==False:
            self.state = 0
            self.timer = 0.0
        
        if self.state==1 and petal.pressed:
            if bop.current_instruction_timer - self.timer >= 200:
                self.state = 0
                return True
        
        return False
    
    def swipe_test(self, bop, ins):
        petal = ins.captouch.petals[self.petal]

        if self.state==0 and petal.pressed:
            if abs(petal.position[0]) >= 1000:
                self.loc = petal.position[0]
                self.state = 1

        if not petal.pressed:
            self.state = 0
        
        if self.state==1 and petal.pressed:
            if abs(petal.position[0])>=1000 and (petal.position[0]*self.loc)<0:
                self.state = 0
                return True
        
        return False
    
    def shake_test(self, bop, ins):
        acc = ins.imu.acc
        acceleration = (acc[0]**2 + acc[1]**2 + acc[2]**2)**0.5
        return acceleration > 15
    
    
    def rub_test(self, bop, ins):
        petal = ins.captouch.petals[self.petal]

        if not petal.pressed:
            self.state = 0
        elif self.state==0:
            if abs(petal.position[1]) >= 500:
                self.loc = petal.position[1]
                self.state = 1
        elif self.state==1:
            if abs(petal.position[1])>=500 and (petal.position[1]*self.loc)<0:
                self.loc = petal.position[1]
                self.state = 2
        elif self.state==2:
            if abs(petal.position[1])>=500 and (petal.position[1]*self.loc)<0:
                self.loc = petal.position[1]
                return True

        
        return False

if __name__ == '__main__':
    # Continue to make runnable via mpremote run.
    st3m.run.run_view(BopIt(ApplicationContext()))
